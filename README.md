# How to run

```bash
export VAULT_ADDR=https://fake.vault.addr
export VAULT_TOKEN=fake_token
export ROLE_NAME=fake_vault_role_name

python3.8 -m venv venv
source venv/bin/activate

pip install -r requirements.txt

python vault_accessors_cleaner.py
```