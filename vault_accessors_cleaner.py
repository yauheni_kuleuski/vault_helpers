
import asyncio
import json
import logging
import os
from typing import List, Optional

from aiohttp import ClientSession, TCPConnector


VAULT_ADDR = os.environ.get('VAULT_ADDR')
VAULT_TOKEN = os.environ.get('VAULT_TOKEN')
ROLE_NAME = os.environ.get('ROLE_NAME')
secret_kwargs = {
    # Environment variables (Global vars, .env file)
    'vault_addr': VAULT_ADDR,
    'vault_token': VAULT_TOKEN,
    'role_name': ROLE_NAME,
    # Changeable parameters
    'connections_limit': 10,    # TCP connections limit in one session
    'accessors_limit': 0        # 0 -> all, 0 < int = limited amount
}


async def get_secret_id_accessors(
    session: ClientSession, 
    **kwargs: dict
) -> Optional[List[str]]:
    """Get Secret ID accessors"""

    base_url, role_name =kwargs.get('vault_addr'),kwargs.get('role_name')
    url = "{base_url}/v1/auth/approle/role/{role_name}/secret-id".format(
        base_url=base_url, role_name=role_name)
    params = {"list": "true"}
    async with session.get(url, params=params) as response:
        try:
            parsed_response = json.loads((await response.read()).decode('utf8'))
            return parsed_response.get('data', {}).get('keys', [])
        except Exception as error:
            logging.error("Error while getting data from '{url}' API endpoint. " \
                  "See an error: {error}".format(url=url, error=error))
            return []


async def delete_secret_id_accessor(
    session: ClientSession,
    secret_id_accessor: str,
    **kwargs: dict
):
    """Delete secret id accessor"""

    base_url, role_name =kwargs.get('vault_addr'),kwargs.get('role_name')
    url = "{base_url}/v1/auth/approle/role/{role_name}/secret-id-accessor/destroy".format(
        base_url=base_url, role_name=role_name)
    payload = {"secret_id_accessor": secret_id_accessor}
    async with session.post(url, data=json.dumps(payload)) as response:
        try:
            parsed_response = json.loads((await response.read()).decode('utf8'))
            logging.info("Accessor '{accessor}' was deleted. {resp}".format(
                accessor=secret_id_accessor, resp=parsed_response))
        except Exception as error:
            logging.error("Error while deleting '{accessor}' Secret ID accessor. " \
                  "See an error: {error}. Response: {resp}".format(
                      accessor=secret_id_accessor, error=error, resp=response))


async def delete_all_secret_id_accessors(
    session: ClientSession,
    accessor_ids_list: List[str],
    **kwargs: dict
):
    """Delete all secret id accessors"""

    tasks = list()
    if accessors_limit := kwargs.get('accessors_limit'):
        accessor_ids_list = accessor_ids_list[:accessors_limit]
    for accessor_id in accessor_ids_list:
        task = asyncio.get_event_loop().create_task(
            delete_secret_id_accessor(session, accessor_id, **kwargs))
        tasks.append(task)
    await asyncio.gather(*tasks)


async def main(**secret_kwargs: dict):
    """Run asyncio event loop"""

    connector = TCPConnector(limit=secret_kwargs.get('secret_kwargs'))
    auth_headers = {"X-Vault-Token": secret_kwargs.get('vault_token')}
    async with ClientSession(read_timeout=900, connector=connector, headers=auth_headers) as session:
        accessor_ids = await get_secret_id_accessors(session, **secret_kwargs)
        if not accessor_ids:
            logging.error("Nothing to clean. Close script.")
            return
        logging.info(f"Have got {len(accessor_ids)} secrer ID accessors. Run cleaning...")
        # await delete_all_secret_id_accessors(session, accessor_ids, **secret_kwargs)
    

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format="%(asctime)s [%(levelname)s] %(message)s (%(filename)s:%(lineno)s)")

    asyncio.run(main(**secret_kwargs))
